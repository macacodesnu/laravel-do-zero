<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
       
       $usuario = "fulano";
       $perfil = "noob";     

       $dados = [
           'usuario' =>$usuario,
           'perfil' => $perfil
       ];

       return view('home',$dados);
    }
}
